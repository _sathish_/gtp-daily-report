class computer:
    def __init__(self,proc,name) : 
        print("constructor") 
        
        self.a=proc
        self.b=name       #special methods or constructor 
    
    def  call(self,proc,ram):
        print("computer details...."+proc,ram)   # proc and ram are varibales or attributes
        print("computer details...."+self.a,self.b)    # it combine the method and variable
    def update(self,proc,ram):
        self.a=proc             #here self act as pointer to the objec  ( obj2  ) since we have tow obj
        self.b=ram
        print(self.a,self.b)
        print("computer details...."+self.a,self.b)    # it combine the method and variable
    def change(self,proc,ram):
        self.a=proc             #here self act as pointer to the objec  ( obj2  ) since we have tow obj
        self.b=ram
        print(self.a,self.b) 
    @classmethod    
    def info(cls,name):
        cls.name=name
        print("inside the class method" + cls.name)     
    @staticmethod
    def statcifun():
        print("inside the static method")      


obj=computer("amd","512gb")  
obj2=computer("i5","128gb")        #creating object to the class
obj2.call("i5","128gb") #calling the method using objects



obj2.update("i5","128gb")  
  #calling update with obj2
obj2.change("i5","128gb") 
print(id(computer))
obj.info("sathish")  
obj2.statcifun()
