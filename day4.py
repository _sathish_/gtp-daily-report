from threading import * 
from time import sleep             #every program has consider one main thread  
                                   #if you want to create any no of thread to execture simulatanously      
class  hello(Thread):

    def run(self):
        for i in range(0,10):
            print("1st thread is running....")
            sleep(2)                               
            ''' output

1st thread is running....
2st thread is running....
1st thread is running....
2st thread is running....
1st thread is running....
1st thread is running....
1st thread is running....
2st thread is running....
1st thread is running....
2st thread is running....     

'''
class hi(Thread):                                          
     def run(self):
         for i in range(0,5):
            print("2st thread is running....")
            sleep(1)
t1=hello()    #creaating objects to class
t2=hi()
t1.start()
t2.start()  #start automatically call the run method

print("done!!!")    #this statement is exectued by main thread


